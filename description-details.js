const peoples = [



{

        "name":"all",

        "imageName":"tower-new.png",

        "title":"Hightime Zurich Tour",

        "subtitle":"All-in-one",

        "description":"<p class='fs_14 cl_black'>Where better to enjoy the artful essence of Hightime Zurich than in the beating heart of the city?</p><p class='fs_14 cl_black'>Our city tour takes you past Zurich’s famous landmarks that you can see on top of the tower in the Glockenspiel, past statues of the Zurich lion,  and, of course, past the best places to view the city through the eyes of all our Parade figures.</p><p class='fs_14 cl_black'>We’ve added a little information on what to see and do as you walk. Why don’t you get started? Zoom in on the map and press the points of interest circles. Let’s go!</p>"

    },

        {

        "name":"Dandy",

        "imageName":"Dandy.png",

        "title":"Dandy (Salesman)",

        "greenbtn" :"Explore Luxury Line on map",
        "whitebtn" :"Explore Luxury Line",

        "subtitle":"The Luxury Line",

        "description":"<p class='fs_14 cl_black'>Welcome to the show! My luxury line is a special curation of the latest looks and the most luxurious of goods in the whole of Zürich. </p><p class='fs_14 cl_black'>I work and shop in one of the most exclusive stores on Bahnhofstrasse - can you guess which one it is? Why not stop and snap me? The clothing brands here are mostly global, but quintessential  Zurich brands and stores are my speciality: from fashion and shoes, watches and jewels to underwear (did you know that Switzerland was the centre of the silk industry?) and skincare - we have it all! </p><p class='fs_14 cl_black'>What about the global brands? No problem. The <a href='uniwebview://action?key=https://www.bahnhofstrasse-zuerich.ch/?lang=EN'>Bahnhofstrasse Association</a> can help you find them</p>"

    },



     {

        "name":"Trader",

        "imageName":"Trader.png",

        "title":"Trader",

        "greenbtn" :"Explore Money Line on map",
        "whitebtn" :"Explore Money Line",

        "subtitle":"The Money Line",

        "description":"<p class='fs_14 cl_black'>Buy! Buy! Sell! Sell! I work in a big Zurich bank. Banks and insurance companies are still the biggest employers in Zürich city centre, don’t you know? Our banks are on Bahnhofstrasse but more towards the lake. The insurance companies are on the west side of the lake and the money line on the map connects them all together. Credit Suisse is the traditional doyen of all banks in Zurich and commands pride of place at the Paradeplatz. so go to the inner courtyard of the building, next to the main entrance, and you can get a snap with me!</p>",





    },

     {

        "name":"Cook",

        "imageName":"Cook.png",

        "title":"Cook",
        "greenbtn" :"Explore Classic Restaurants on map",
        "whitebtn" :"Explore Classic Restaurants",

        "subtitle":"Classic Restaurants",

        "description":"<p class='fs_14 cl_black'>Feeling peckish? Zurich people are big foodies. There are good restaurants open everywhere (except on Sundays).</p><p class='fs_14 cl_black'>Though we do love Italian cuisine in the city, you can find some delicious dishes in one of the real old Swiss restaurants on my map. We love Egli filets in the summer and Züri geschnetzeltes in the wintertime. Come and check out the interiors and exteriors of these fine old restaurants, and if I can get out of the kitchen, why not get a snap with me?</p><p class='fs_14 cl_black'>The Guildhouses all have traditional Swiss restaurants, so you may find me there too (see the Sechseläuten map).</p>",





    },

     {

        "name":"Roadworker",

        "imageName":"Roadwork.png",

        "title":"Roadworker",
        "greenbtn" :"Explore Roadworks on map",
        "whitebtn" :"Explore Roadworks",

        "subtitle":"",

        "description":"<p class='fs_14 cl_black'>Zürich roads are always in perfect condition, thanks to me, and I like to keep them that way by making sure they’re always being repaired. Yeah, yeah extensive roadworks are an obstacle that Zürich residents and my friends at Hightime have to get around. Roads are a work of art, you know?</p><p class='fs_14 cl_black'>Major roadworks in the city centre are pretty rare at the moment, so you’ll have to try really hard to find me. Perhaps I’m digging over at Talstrasse, on the Post Brücke near the Hauptbahnhof or just down on the Limmatquai. Don’t be late!</p>",





    },

     {

        "name":"Dog",

        "imageName":"Dog.png",

        "title":"Dog",
        "greenbtn" :"Explore Green Routes on map",
        "whitebtn" :"Explore Green Routes",

        "subtitle":"Green routes",

        "description":"<p class='fs_14 cl_black'>Woof! Woof! Dogs like me are everywhere in Zürich but we don’t have a park to run about and exercise in. </p><p class='fs_14 cl_black'>If you want to follow me, try the Seefeldquai between Bellevue and the Zürihorn. Or, jump on tram 5 and head for the zoo. You can walk me through the woods from there. Don’t forget that dogs like me need a ticket on Zürich trams and we’re not even allowed in the zoo. But in Hightime I ride for free and can go anywhere. Woof! Woof!</p>",





    },

     {

        "name":"Raver",

        "imageName":"Raver.png",

        "title":"Raver",
        "greenbtn" :"Explore Street Parade Route on map",
        "whitebtn" :"Explore Street Parade Route",

        "subtitle":"Street Parade Route",

        "description":"<p class='fs_14 cl_black'>I’m a passionate raver so you will always find me at the street parade, Europe’s biggest techno street party. </p><p class='fs_14 cl_black'>The street parade route will take you around the city-end of the lake. The real parade takes place once a year in August: it stretches from east to west, but why not take the other direction to stay in the sun? Just like the history of the street parade, the statues that border the route exhibit love as well as youthful courage. Snap me next to Aphrodite, Ganymede or David.</p>",





    },

     {

        "name":"Guildsman",

        "imageName":"Guildsman.png",

        "title":"Guildsman",

        "greenbtn" :"Explore Sechseläuten Route on map",
        "whitebtn" :"Explore Sechseläuten Route",

        "subtitle":"Sechseläuten Route",

        "description":"<p class='fs_14 cl_black'>Can you hear the Sechseläuten March in the Hightime Parade? That’s Zurich’s signature tune. Zurich’s Sechseläuten celebrates the end of winter. Why not follow the parade route as it goes around the city and ends at the Opera House? </p><p class='fs_14 cl_black'>It is there that we ride around the Böögg, which is a snowman on top of a bonfire designed to explode! If the snowman explodes quickly after the bonfire is lit, then it will be a good summer. <p><p class='fs_14 cl_black'>Why not stop and get a snap of me in front of a guildhouse, near the Opera, or on my way there with my e-scooter?</p><p class='fs_14 cl_black'>You can find out more about <a href='uniwebview://action?key=https://en.wikipedia.org/wiki/Z%C3%BCnfte_of_Z%C3%Bcrich'>Zurich guilds and guildhouses here</a></p>",





    },

     {

        "name":"Beachgirl",

        "imageName":"beachgirl.png",

        "title":"Beachgirl",
        "greenbtn" :"Explore Water Line on map",
        "whitebtn" :"Explore Water Line",

        "subtitle":"Swimming",

        "description":"<p class='fs_14 cl_black'>Did you know that Switzerland’s lakes and rivers are some of the cleanest in the world? Wow! They say if you live in Zürich, stay in the city in summer and take your vacation some other time. We are proud of our “Badi” tradition and several are near the centre of town where I can go before or after work and even at lunchtime. I like the after swim bar near Burkliplatz: perfect for a sneaky cosmo! </p><p class='fs_14 cl_black'>Shall we get a selfie while I’m dressed for the sun? And after, you can <a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>go for a swim</a> yourself or get on the water with <a href='uniwebview://action?key=https://www.zsg.ch/en/unique-cruises'>a lake cruise</a>.</p>",





    },

     {

        "name":"Rower",

        "imageName":"rover.png",

        "title":"Rower",

        "greenbtn" :"Explore Water Line on map",
        "whitebtn" :"Explore Water Line",

        "subtitle":"Boathouses",

        "description":"<p class='fs_14 cl_black'>To be a competitive rower, you need strong arms and a powerful body. I like to stay trim.</p><p class='fs_14 cl_black'>Lake Zurich is 40 km long and the ideal rowing spot in the northern tip which is part of the city. That’s where all rowing clubs cohabit and it’s a tiny patch of land! The Grasshopper Club Rowers have used what little space is available to build a giant palace of a boat house, why not grab a pic with me onshore? I’ll bring my oars. The view of the lake in the mountains is spectacular from there and if you want to <a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>go for a swim</a> nearby, go for it! Or why not get on the water with <a href='uniwebview://action?key=https://www.zsg.ch/en/unique-cruises'>a lake cruise</a>.</p>",





    }



    ]



    console.log(peoples);

   







