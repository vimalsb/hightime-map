var map = L.map('map', {
	minZoom: 13,
	maxZoom: 18,
	zoomDelta: 0.25,
	zoomSnap: 0.2
});

L.tileLayer('https://api.mapbox.com/styles/v1/rengars/ckojqr0pr018u18ozl9tww43p/tiles/512/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoicmVuZ2FycyIsImEiOiJja2k1enJ6dmszcTZxMnJwNTFnOGZhbGVtIn0.L8YRslW4rVaCFfqFKAvLvA', {
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1,
		attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors',
	}).addTo(map);

	var LeafIcon = L.Icon.extend({
		options: {
			//shadowUrl: 'leaf-shadow.png',
			iconSize:     [15, 15],
			shadowSize:   [15, 15],
			iconAnchor:   [15, 15],
			shadowAnchor: [4, 62],
			popupAnchor:  [-3, -76]
		}
	});
	const green = 'https://animateyourcity.com/images/pointer/green.png';
	const blue = 'https://animateyourcity.com/images/pointer/blue.png';
	const yellow = 'https://animateyourcity.com/images/pointer/yellow.png';
	const red = 'https://animateyourcity.com/images/pointer/red.png';
	const seagreen = 'https://animateyourcity.com/images/pointer/dog-green.png';
	const purple = 'https://animateyourcity.com/images/pointer/purple.png';
	const grey = 'https://animateyourcity.com/images/pointer/grey.png';
	const black = 'https://animateyourcity.com/images/pointer/black.png';
	const darkblue = 'https://animateyourcity.com/images/pointer/dark-blue.png';
	const ferry = 'https://animateyourcity.com/images/pointer/ferry.png';
	const transparent = 'https://animateyourcity.com/images/pointer/transparent.png';
	
	const lineColorGreen = "#00b050";
	const lineColorDogGreen = "#92d050";
	const lineColorRed = "red";
	const lineColorBlue = "#4472c4";
	const lineColorDBlue = "#002060";
	const lineColorPurple = "purple";
	const lineColorYellow = "#ffff00";
	const lineColorBlack = "black";
	const lineColorGrey = "#878787";
	const lineColorAqua = "aqua";
	const lineColorOrange = "orange";
	const lineColorcook = "#0000";


	let categorys = {

		"Dandy1":[
		{
			"coordiates":[47.35661778898828, 8.53612361950723],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all1",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.35644339334677, 8.535741319581707],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all2",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.35959143497018, 8.535465790013488],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all3",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.363462663787665, 8.535180882208932],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all4",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.366333149441246, 8.540502431740004],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all5",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.36678025718383, 8.540398912060134],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all6",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.369742690165815, 8.539359406032897],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all8",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.37240206473725, 8.538308005327279],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all9",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.37387318992184, 8.538351549386276],
			"icon":green,
			"lineColor" : lineColorRed,
			"pointernae":"all7",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"1"
		},
		{
			"coordiates":[47.37577822608385, 8.539227985121915],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all10",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"	
		},//47.3745666251704, 8.538672347661123
		{
			"coordiates":[47.3746384026499, 8.538753948196298],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all101",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"	
		},
		{
			"coordiates":[47.374142857456455, 8.538713024823526],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all11",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"	
		},
		{
			"coordiates":[47.37344753096996, 8.539242744013245],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all111",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"	
		},
		{
			"coordiates":[47.37231209556924, 8.540206220970386],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all112",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"	
		},
		{
			"coordiates":[47.3721451177174, 8.540851067742246],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all12",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"	
		},
		{
			"coordiates":[47.37170345264268, 8.54093335909596],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all14",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.3709300167948, 8.541120386103502],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all141",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		// {
		// 	"coordiates":[47.371071648710775, 8.5412685307699],
		// 	"icon":green,
		// 	"lineColor" : lineColorGreen,
		// 	"pointernae":"all15",
		// 	"popup":"all popup",
		// 	"is_waypoints":"1",
		// 	"is_all_waypoints":"2"
		// },
		{
			"coordiates":[47.370872740864584, 8.541351676409802],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all16",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.37021061324518, 8.541094185156503],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all17",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
			
		},
		{
			"coordiates":[47.37007982167663, 8.540404857275965],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all18",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
			
		},
		{
			"coordiates":[47.36989634922071, 8.542145610797665],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all19",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
			
		},
		{
			"coordiates":[47.37006522586506, 8.543327109127777],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all20",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
			
		},
		{
			"coordiates":[47.3709608442266, 8.542948716391646],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all201",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
			
		},
		{
			"coordiates":[47.37155484209301, 8.542876512998912],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all21",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"	
		},
		{
			"coordiates":[47.37174148726118, 8.543824673971812],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all22",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.37230641500639, 8.543864907049542],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all23",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.37250077751209, 8.545235515711099],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all24",
			"popup":"all pop",
			"is_waypoints":"1",
			"is_all_waypoints":"2"	
		},
		{
			"coordiates":[47.37250689990771, 8.545708105843238],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all241",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"	
		},
		{
			"coordiates":[47.37287382436145, 8.546255270489324],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all242",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"	
		},
		{

			"coordiates":[47.37305728623345, 8.547330836311534],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all25",
			"popup":"Rechberg",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.37276302890695, 8.547620478033112],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all26",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{

			"coordiates":[47.37273071884867, 8.547798847751531],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all27",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{

			"coordiates":[47.37211673086625, 8.548705386506281],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all271",
			"popup":"Florhof",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{

			"coordiates":[47.37156996651599, 8.547721015801995],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all28",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.370994164325566, 8.54806700482164],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all29",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.37087430810479, 8.548276212169466],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all291",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.370682299613826, 8.548352747754597],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all292",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.370673, 8.548222],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all293",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.370589, 8.548270],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all294",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.370527, 8.547894],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all295",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.370622, 8.547680],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all296",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.370794346518494, 8.54759761825157],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all30",
			"popup":"Kunsthaus",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.37088825857516, 8.54706485531669],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all31",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.370770761689, 8.5469243961544],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all32",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.37073988067387, 8.546685679555909],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all321",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.3699169857856, 8.547066553229907],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all322",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.369793459605624, 8.546736641528657],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all323",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.369827974299575, 8.546438916332786],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all324",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.36911587707634, 8.544952972528364],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all33",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.3682130257121, 8.545886381262312],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all331",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.36790238313471, 8.546218975181363],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all332",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.36689779351199, 8.544156381126255],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all34",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.361824342643594, 8.54729270011266],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all341",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.35825562539191, 8.54814144837504],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all342",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.353168391784806, 8.552314785054762],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all35",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.352714458790864, 8.557489991517594],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all36",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.350027580888174, 8.560847966168563],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all37",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.352714458790864, 8.557489991517594],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all371",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.353168391784806, 8.552314785054762],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all372",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.35825562539191, 8.54814144837504],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all373",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.361824342643594, 8.54729270011266],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all374",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.36689779351199, 8.544156381126255],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all375",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.36705769321151, 8.543943123499014],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"all38",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		},
		{
			"coordiates":[47.36655182643231, 8.540521694830174],
			"icon":green,
			"lineColor" : lineColorGreen,
			"pointernae":"39",
			"popup":"all popup",
			"is_waypoints":"1",
			"is_all_waypoints":"2"
		}
		],

		"parade":[
		{

			"coordiates":[47.379193793447314, 8.540527747801566],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Landesmuseum",
			"popup":"<h1>Landesmuseum</h1><h2>Museumstrasse, 2, 8001</h2><img src='images/cha/parade/Landsemuseum-L.jpg'/><p><b>Fun Tips:</b> Did you find the Einfach Zürich (Simply Zurich) section of the museum?</p><p><a href='uniwebview://action?key=https://www.landesmuseum.ch/en'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37111371114692, 8.540744395894432],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"St Peter",
			"popup":"<h1>St Peter</h1><h2>St Peterhofstatt, 1, 8001</h2><img src='images/cha/parade/St-Peter-P2.jpg'/><p><b>Fun Tips:</b> Look up at the tower clockface it's the largest in Europe. It's also the official time of Zurich. Wow!</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/attractions/st-peter'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.370629727337565, 8.541918656153006],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Wühre",
			"popup":"<h1>Wühre</h1><h2>Wühre, 7, 8001</h2><img src='images/cha/parade/Wuhre-P.jpg' /><p><b>Fun Tips:</b> Savour the views across the Limmat river</p><p><a href='uniwebview://action?key=https://www.chocolatdietermeier.com/'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.36973215372447, 8.541580196355532],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Fraumünster",
			"popup":"<h1>Fraumünster</h1><h2>Münsterhof, 2, 8001</h2><img src='images/cha/parade/Fraumunster-L2.jpg'/><p><b>Fun Tips:</b> Go inside for the Chagall windows</p><p><a href='uniwebview://action?key=https://en.wikipedia.org/wiki/Fraum%C3%BCnster'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
// 		{

// 			"coordiates":[47.37158504022299, 8.54311998148217],
// 			"icon":grey,
// 			"lineColor" : lineColorGrey,
// 			"pointernae":"Haue",
// 			"popup":"<h1>Zunfthaus zur Haue</h1><h2>Limmatquai, 52, 8001</h2><img src='images/cha/parade/GLOBUS L.jpg'/><p><b>Fun Tips:</b> Savour the views across the Limmat river</p>",
// 			"is_waypoints":"0",
// "is_all_waypoints":"0"
			
// 		},
// {

// 			"coordiates":[47.37108304093755, 8.54324242459495],
// 			"icon":grey,
// 			"lineColor" : lineColorGrey,
// 			"pointernae":"Rüden",
// 			"popup":"<h1>Haus zum Rüden</h1><h2>Limmatquai, 42, 8001</h2><img src='images/cha/parade/GLOBUS L.jpg'/><p><b>Fun Tips:</b> See the wooden barrel-vaulted ceiling in the Gotischer Saal with a fine view over the Limmat. Place the hightime Cook</p>",
// 			"is_waypoints":"0",
// "is_all_waypoints":"0"
			
// 		},
		{

			"coordiates":[47.37024564854137, 8.544075464000239],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Grossmünster",
			"popup":"<h1>Grossmünster</h1><h2>Grossmünsterplatz, 8001</h2><img src='images/cha/parade/Grossmunster-L.jpg'/><p><b>Fun Tips:</b> Go inside for the windows by Sigmar Polke and go round the back to visit the restored cloister.</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/attractions/grossmuenster-church'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37051277272987, 8.548223984934737],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Kunsthaus",
			"popup":"<h1>Kunsthaus</h1><h2>Heimplatz, 1, 8001</h2><img src='images/cha/parade/Kunsthaus-P.jpg'/><p><b>Fun Tips:</b> Visit the museum, whose big new extension designed by David Chipperfield opens later this year. Fly the hightime tower outside the museum.</p><p><a href='uniwebview://action?key=https://www.kunsthaus.ch/en/'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37312057233375, 8.547742106810125],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Rechberg",
			"popup":"<h1>Haus zum Rechberg</h1><h2>Hirschengraben, 40, 8001</h2><img src='images/cha/parade/Rechberg-P.jpg'/><p><b>Fun Tips:</b> Did you know this house is the seat of Zurich's cantonal government? Are you on your best behaviour? Go to the garden and at the top, see Undine by notorious Zurich graffiti artist Harald Naegeli.</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/nature/rechberg'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.36527739879318, 8.54630220863508],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Opera",
			"popup":"<h1>Opera</h1><h2>Sechseläutenplatz, 1, 8008</h2><img src='images/cha/parade/Opera-House.jpg'/><p><b>Fun Tips:</b> Sit on the Sechseläutenplatz in front of the Opera House and watch the comings and goings. View the Glockenspiel Parade against the trees on the Stadelhoferplatz nearby</p><p><a href='uniwebview://action?key=https://www.opernhaus.ch/en/about-us/history-and-architecture/'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37105004880837, 8.539034473073391],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Leuenhof",
			"popup":"<h1>Leuenhof (building embellished with lions)</h1><h2>Bahnhofstrasse, 32, 8001</h2><img src='images/cha/parade/Leuenhof-P.jpg'/><p><b>Fun Tips:</b> Find as many lions on the buildng as you can</p><p><a href='uniwebview://action?key=https://www.youtube.com/watch?v=7LLwlN3eIbM'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},{

			"coordiates":[47.37167367811796, 8.542612533404075],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Rathaus",
			"popup":"<h1>Rathaus (building decorated with lions)</h1><h2>Limmatquai, 55, 8001</h2><img src='images/cha/parade/RATHAUS-P.jpg'/><p><b>Fun Tips:</b> See the golden lions next to the door. Enjoy the many busts of historical and legendary figures over the ground floor windows</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/attractions/rathaus-zuerich'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},{

			"coordiates":[47.35962777325516, 8.537150217252787],
			"icon":grey,
			"lineColor" : lineColorGrey,
			"pointernae":"Löwendenkmal",
			"popup":"<h1>Löwendenkmal (Lion statue)</h1><h2>Mythenquai, Hafen Enge, 8002</h2><img src='images/cha/parade/Lowendenkmal-L.jpg'/><p><b>Fun Tips:</b> Get a good view of the mountains next to the Lion statue in the harbour. Try Kellers, new at the Hafen Beiz Enge Kiosk/restaurant.</p><p><a href='uniwebview://action?key=https://blog.nationalmuseum.ch/en/2019/02/urs-eggenschwyler-the-lion-jester-of-zurich/'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
			
		},

		],
		"Dandy":[
		{

			"coordiates":[47.36787948662207, 8.539937992490522],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Dandy1",
			"popup":"all popup",
			"is_waypoints":"1",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.36993223985974, 8.539272803140268],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Dandy2",
			"popup":"2test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.3723736346859, 8.538317940843143],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Dandy3",
			"popup":"3test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37386527125836, 8.538330283113378],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Dandy31",
			"popup":"3test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37457152531971, 8.538650529499407],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Dandy4",
			"popup":"3test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37690009824256, 8.539857524530218],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Dandy5",
			"popup":"3test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.370555378061894, 8.538309852261136],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Fabric Frontline",
			"popup":"<h1>Fabric Frontline</h1><h2>Bahnhofstr 25, 8001</h2><img  src='images/cha/danndy/FABRIC-FRONTLINE-L.jpg'/><p><b>Fun Tips:</b> Ogle the silk foulards</p><p><a href='uniwebview://action?key=https://www.fabricfrontline.ch/en/'>For more information</a></p>",
			"is_waypoints":"0",
            "is_all_waypoints":"0"
		},
		{

			"coordiates":[47.375909750252404, 8.537819930151265],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Globus",
			"popup":"<h1>Globus</h1><h2>Schweizergasse 11, 8001</h2><img src='images/cha/danndy/GLOBUS-L.jpg'/><p><b>Fun Tips:</b> Go for the food</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/shopping/globus-bahnhofstrasse'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.370457024123006, 8.53961452830002],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Grieder",
			"popup":"<h1>Grieder</h1><h2>Bahnhofstr 30, 8001</h2><img src='images/cha/danndy/GRIEDER-L.jpg'/><p><b>Fun Tips:</b> Try the rooftop bar</p><p><a href='uniwebview://action?key=https://stores.bongenie-grieder.ch/1031903-grieder-zurich'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37435982621815, 8.537320655692408],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Jelmoli",
			"popup":"<h1>Jelmoli</h1><h2>Seidengasse 1, 8001</h2><img src='images/cha/danndy/Jelmoli-L.jpg'/><p><b>Fun Tips:</b> Seek out the Swiss brands. Underwear: Blue Lemon, Calida, Fogal, Hanro, Triumph, Zimmerli. Shoes: Bally, Benci Brothers, On. Skincare: La Prairie, Valmont, Cellcosmet </p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/shopping/jelmoli'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37503089763825, 8.539176417334188],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Kurz",
			"popup":"<h1>Kurz</h1><h2>Bahnhofstr 80, 8001</h2><img src='images/cha/danndy/KURZ-L.jpg'/><p><b>Fun Tips:</b> Go for the Glockenspiel in front of the store that plays at 11.00 and 14.00. Play the hightime Glockenspiel next to it.</p><p><a href='uniwebview://action?key=https://kurz1948.ch/en/'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37468541601589, 8.538932506814943],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"Modissa",
			"popup":"<h1>Modissa</h1><h2>Bahnhofstr 74, 8001</h2><img src='images/cha/danndy/Modissa-L.jpg'/><p><b>Fun Tips:</b> Rise to the rooftop restaurant (separately run from the store)</p><p><a href='uniwebview://action?key=http://www.peclard.net/en/our-companies/peclard-chez-modissa/'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.375567434516476, 8.539354858922394],
			"icon":red,
			"lineColor" : lineColorRed,
			"pointernae":"PKZ Women",
			"popup":"<h1>PKZ Women</h1><h2>Bahnhofstr 88, 8001</h2><img src='images/cha/danndy/PKZ-L.jpg'/><p><b>Fun Tips:</b> Watch the Julian Opie video installation in front of the store</p><p><a href='uniwebview://action?key=https://www.bahnhofstrasse-zuerich.ch/Modissa'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		}
		],
		"Raver":[
			{

			"coordiates":[47.36191881440891, 8.547143894365517],
			"icon":purple,
			"lineColor" : lineColorPurple,
			"pointernae":"Raver1",
			"popup":"<h1>Aphrodite statue</h1><h2>Street : Near General Guisan Quai </h2> <h2>Street no.: 37, Postal code: 8002</h2>",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36654790673802, 8.544515326243971],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver2",
			"popup":"2test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.3670945585057, 8.543939474684397],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver20",
			"popup":"2test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36660042565368, 8.54172933449162],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver21",
			"popup":"2test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36573880850843, 8.539777880132],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver3",
			"popup":"3test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36444944045175, 8.536965731311852],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver31",
			"popup":"3test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.3623596623052, 8.535636526485991],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver4",
			"popup":"3test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.35921728292755, 8.536052149772795],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Raver5",
			"popup":"3test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.364160, 8.536977],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Aphrodite",
			"popup":"<h1>Aphrodite statue</h1><h2>Near General Guisan Quai 32, 8002</h2><img  src='images/cha/raver/aphrodite-L.jpg'/><p><b>Fun Tips:</b> Make some shapes with Aphrodite. There's a great place to crash and watch the mountains too!</p><p><a href='uniwebview://action?key=https://en.wikipedia.org/wiki/Einar_Utzon-Frank'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36605233407378, 8.541260124965248],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"Ganymede",
			"popup":"<h1>Ganymede statue</h1><h2>Bürkliplatz, 8001</h2><img  src='images/cha/raver/ganymede-L.jpg'/><p><b>Fun Tips:</b> So much to see after a bop with Ganymede. Take a short cruise on the lake, starting from the Bürkliplatz pier nearby, to see bathers, rowers, sailors and more on the 'Zürisee'</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/attractions/ganymed'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.365241997084965, 8.54553183570942],
			"icon":purple,
"lineColor" : lineColorPurple,
			"pointernae":"David",
			"popup":"<h1>David statue</h1><h2>Sechseläutenplatz, 8001</h2><img  src='images/cha/raver/david-P1.jpg'/><p><b>Fun Tips:</b> Dance under the statue of David like we do at the Streetparade! Get a snap with me here!</p><p><a href='uniwebview://action?key=https://www.christies.com/en/lot/lot-4799623'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		}

		],
		"Trader":[
			{
	
				"coordiates":[47.35923382950216, 8.53603993197039],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader1",
				"popup":"<h1>UBS</h1><h2>Street : Bahnhofstrasse. </h2> <h2>Street no.: 45, Postal code: 8001</h2>",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.35921728292755, 8.536052149772795],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader2",
				"popup":"1test",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.3623596623052, 8.535636526485991],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader3",
				"popup":"1test",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36443277533463, 8.536977117066218],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader4",
				"popup":"1test",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.366351863844045, 8.540496908766833],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader5",
				"popup":"1test",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.3668040369664, 8.540330007255504],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader6",
				"popup":"1test",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.369742690165815, 8.539359406032897],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader7",
				"popup":"1test",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37240206473725, 8.538308005327279],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Trader8",
				"popup":"1test",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37206080909378, 8.538207393210707],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"UBS",
				"popup":"<h1>UBS</h1><h2>Bahnhofstrasse 45, 8001</h2><img  src='images/cha/trader/UBS-L.jpg'/><p><b>Fun Tips:</b> Look for the John Armleder painting in the recently renovated banking hall</p><p><a href='uniwebview://action?key=https://www.ubs.com/global/en/investor-relations/annual-review/2018/spotlight/bahnhofstrasse45.html'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37011591477958, 8.538622849609295],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"CS",
				"popup":"<h1>Credit Suisse</h1><h2>Paradeplatz 8, 8001</h2><img  src='images/cha/trader/CREDIT-SUISSE-P.jpg'/><p><b>Fun Tips:</b> Go into the internal courtyard of the Credit Suisse building (entry from Paradeplatz is right in middle of Credit Suisse buiding)and view the hightime Trader. Then cross the Paradeplatz, have a coffee in Sprüngli opposite and watch the comings and goings</p><p><a href='uniwebview://action?key=https://www.credit-suisse.com/about-us/en/our-company/who-we-are.html#:~:text=It%20all%20began%20with%20Swiss,50%20countries%20around%20the%20world.'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36832572054428, 8.539397607417126],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"ZKB",
				"popup":"<h1>Zurich Cantonal Bank</h1><h2>Bahnhofstrasse 9, 8001</h2><img  src='images/cha/trader/ZKB-L.jpg'/><p><b>Fun Tips:</b> Use the free 'Büro Züri' coworking space in the Zurich Cantonal Bank headquarters. The hightime Trader is probably using it. Maybe the Raver is using it too?</p><p><a href='uniwebview://action?key=https://www.zkb.ch/en/home/our-company.html'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36784023285354, 8.540630934647584],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"SNB",
				"popup":"<h1>Swiss National Bank</h1><h2>Börsenstrasse 15, 8001</h2><img  src='images/cha/trader/SNB-L.jpg'/><p><b>Fun Tips:</b> View the interactive media wall and visit the walk-in gallery to learn the history of the Swiss National Bank </p><p><a href='uniwebview://action?key=https://www.snb.ch/en/iabout/snb/address/id/snb_addresses_forum'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.359994517901605, 8.534820663192868],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Swiss Re",
				"popup":"<h1>Swiss Re</h1><h2>Mythenquai 50/60, 8002</h2><img  src='images/cha/trader/Swiss-Re-L1.jpg'/><p><b>Fun Tips:</b> Explore reflections of the lake in the waved glass front of the 2017 Swiss Re Next building by architects Diener and Diener </p><p><a href='uniwebview://action?key=https://www.swissre.com/about-us/our-global-presence/campus-mythenquai/zurichs-enge-district-part-1.html'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36229027358674, 8.535049164922365],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Zurich",
				"popup":"<h1>Zurich</h1><h2>Mythenquai 2, 8002</h2><img  src='images/cha/trader/Zurich-L.jpg'/><p><b>Fun Tips:</b> See the Springbrunnen in the lake. Sponsored by Zurich insurance and designed by Andres Bosshard, this fountain has water jets whose rhythm is aligned with the earth's vibrations.</p><p><a href='uniwebview://action?key=https://www.zurich.com/en/about-us/offices/quai-zurich'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.36298521245633, 8.53469255828037],
				"icon":blue,
				"lineColor" : lineColorBlue,
				"pointernae":"Swiss Life",
				"popup":"<h1>Swiss Life</h1><h2>General Guisan Quai 40, 8022</h2><img  src='images/cha/trader/Swiss-Life-L.jpg'/><p><b>Fun Tips:</b> Walk over to the Volière. Swiss Life looks after people's lives. The Volière, close by Swiss Life on the lake side of the Mythenquai, looks after birds' lives. It is a bird hospital. The highlife Dog may be there having a good look at his feathered friends</p><p><a href='uniwebview://action?key=https://www.swisslife.com/en/home/about-us/our-history.html'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			}
			
			],
			"Cook":[
			{
	
				"coordiates":[47.36772605584657, 8.545791464001539],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Cook1",
				"popup":"1test",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
			},

			{
	
				"coordiates":[47.37249303672568, 8.548765293294448],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Cook2",
				"popup":"1test",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.3728543127033, 8.546176555775563],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Cook3",
				"popup":"1test",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.373561073445124, 8.536372956555041],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Cook4",
				"popup":"1test",
				"is_waypoints":"1",
	"is_all_waypoints":"0"
			},
	// 		{
	
	// 			"coordiates":[47.37249303672568, 8.548765293294448],
	// 			"icon":green,
	// 			"lineColor" : lineColorcook,
	// 			"pointernae":"Cook5",
	// 			"popup":"1test",
	// 			"is_waypoints":"1",
	// "is_all_waypoints":"0"
	// 		},
	// 		{
	
	// 			"coordiates":[47.37182070851623, 8.536362507420666],
	// 			"icon":green,
	// 			"lineColor" : lineColorcook,
	// 			"pointernae":"Cook6",
	// 			"popup":"1test",
	// 			"is_waypoints":"1",
	// "is_all_waypoints":"0"
	// 		},
			 {
	
				"coordiates":[47.36772605584657, 8.545791464001539],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Kronenhalle",
				"popup":"<h1>Kronenhalle</h1><h2>Rämistr 4, 8001</h2><img src='images/cha/cook/Kronenhalle-L.jpg'/><p><b>Fun Tips:</b> See the paintings in the restaurant. Get there early and have a drink in the bar.</p><p><a href='uniwebview://action?key=https://www.kronenhalle.ch/?LG=EN'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37237629967312, 8.544607251278492],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Oepfelchammer",
				"popup":"<h1>Oepfelchammer</h1><h2>Rindermarkt 12, 8001</h2><img src='images/cha/cook/Oepfelchammer-L.jpg'/><p><b>Fun Tips:</b> Eat in the Weinstube Oeli upstairs. After a few glasses of wine, try jumping between one of the wooden beams and the ceiling and exiting on the other side of the beam. It is a traditional but extremely difficult trick to pull off. Be careful! Ask the waiters.</p><p><a href='uniwebview://action?key=https://www.oepfelchammer.ch/en/'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.372798416363544, 8.545752240961873],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Neumarkt",
				"popup":"<h1>Neumarkt</h1><h2>Neumarkt 5, 8001</h2><img  src='images/cha/cook/Neumarkt-L.jpg'/><p><b>Fun Tips:</b> In high summer, try the cool hidden garden at the back.</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/restaurants/neumarkt'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37247166517701, 8.540644592660863],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Hotel Kindli",
				"popup":"<h1>Kindli</h1><h2>Pfalzgasse 1, 8001</h2><img  src='images/cha/cook/KINDLI-L.jpg'/><p><b>Fun Tips:</b> Buy a dancing hare at En Soie next door. Exit the shop, turn right, go across the road and, opposite the fountain, look up to see two En Soie hares dancing on the roof.</p><p><a href='uniwebview://action?key=https://www.kindli.ch/?LG=EN'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37223751830927, 8.54873749396196],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Hotel Florhof",
				"popup":"<h1>Florhof</h1><h2>Florhofgasse 4, 8001</h2><img  src='images/cha/cook/Florhof-L.jpg'/><p><b>Fun Tips:</b> Eat outside in the pretty courtyard.</p><p><a href='uniwebview://action?key=https://hotelflorhof.ch/?lang=en'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.37182070851623, 8.536362507420666],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Kaufleuten",
				"popup":"<h1>Kaufleuten</h1><h2>Pelikanplatz 18, 8001</h2><img  src='images/cha/cook/Kaufleuten-L.jpg'/><p><b>Fun Tips:</b> Restaurant, Club, and Cultural Program, go for the food, stay for the fun.</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/restaurants/kaufleuten'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.370976744567464, 8.54125562513764],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Veltlinerkeller",
				"popup":"<h1>Veltlinerkeller</h1><h2>Schlüsselgasse 8, 8001</h2><img  src='images/cha/cook/Veltliner-Keller-L.jpg'/><p><b>Fun Tips:</b> Ask to view the wine cellar</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/restaurants/veltlinerkeller'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			},
			{
	
				"coordiates":[47.373263247782724, 8.536700989095396],
				"icon":green,
				"lineColor" : lineColorcook,
				"pointernae":"Hiltl",
				"popup":"<h1>Hiltl </h1><h2>St Anna Gasse 18, 8001</h2><img  src='images/cha/cook/HILTL-L.jpg'/><p><b>Fun Tips:</b> The oldest vegetarian restaurant in the world. Buy your food by the kilo!</p><p><a href='uniwebview://action?key=https://hiltl.ch/en/'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
				
			}	
	
			],
			"Dog":[
		{
			"coordiates":[47.38222154139295, 8.571709249405291],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train17",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.379170392755455, 8.570422191631994],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train16",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.37873213553508, 8.569703763080057],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train15",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.37841220118346, 8.566927187383664],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train14",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.379407041639354, 8.559943671491382],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train13",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.378850473037495, 8.559102319771698],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train12",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.37700532977674, 8.559730137789522],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train11",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.3755283151962, 8.55855861394079],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train10",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},	
		{
			"coordiates":[47.37537492685848, 8.557879037447242],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train9",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},	
		{
			"coordiates":[47.376312863762074, 8.55580796070538],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train8",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.37680372617565, 8.554843639165764],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train7",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.37710616472855, 8.55267541955001],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train6",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.37599729702353, 8.553393830764845],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train5",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{
			"coordiates":[47.37478761261234, 8.552015243236694],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train4",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},		
		{
			"coordiates":[47.37462982360718, 8.549633504682333],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train2",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},	
			{
			"coordiates":[47.37207886870736, 8.550636676848717],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train1",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
			{

			"coordiates":[47.36835739124954, 8.547122299732871],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog-train0",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
		"is_all_waypoints":"0"
			},
		{

			"coordiates":[47.36724873228388, 8.54503766189074],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog1",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Street : Bellevue  </h2> ",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36650192442704, 8.544351761539165],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog2",
			"popup":"2test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.3614910815177, 8.547392939891346],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog3",
			"popup":"3test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.35807200808734, 8.547736208994438],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog4",
			"popup":"3test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.35310844649945, 8.552288286520369],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Dog5",
			"popup":"3test",
			"is_waypoints":"1",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36724873228388, 8.54503766189074],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Bellevue",
			"popup":"<h1>Bellevue (Trams)</h1><h2>Bellevueplatz, 8001</h2><img  src='images/cha/dog/Bellevue-L.jpg'/><p><b>Fun Tips:</b> Have a sausage, mustard, and a bread roll in the Vorderer Sternen. In the middle of the trams, have a coffee at the bar in the Caffè & Bar Bellevue and watch the tram ballet.</p><p><a href='uniwebview://action?key=https://en.wikipedia.org/wiki/Bellevueplatz'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.353168391784806, 8.552314785054762],
			"icon":seagreen,
            "lineColor" : lineColorDogGreen,
			"pointernae":"Heureka",
			"popup":"<h1>Heureka von Jean Tinguely</h1><h2>nr Zürichhorn, 8008</h2><img  src='images/cha/dog/Zurichhorn-Eureka-L.jpg'/><p><b>Fun Tips:</b> Enjoy the mobile sculpture Heureka by Jean Tinguely and the view of the mountains</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/attractions/heureka-jean-tinguely'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.381646245724426, 8.571352649009718],
			"icon":seagreen,
			"lineColor" : lineColorDogGreen,
			"pointernae":"Zoo",
			"popup":"<h1>Zoo</h1><h2>Zürichbergstrasse, 8044</h2><img  src='images/cha/dog/Zoo-Zurichberg-L.jpg'/><p><b>Fun Tips:</b> See the excellent Zoo (no dogs allowed except hightime Dog). See James Joyce grave in nearby Fluntern cemetery.</p><p><a href='uniwebview://action?key=https://www.zoo.ch/en'>For more information</a></p>",
			"is_waypoints":"0",
"is_all_waypoints":"0"
		}

		],
		
	  "Guildsman":[
		{
	
			"coordiates":[47.36665437903424, 8.540378663630802],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman1",
			"popup":"1test",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.369742690165815, 8.539359406032897],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman2",
			"popup":"1test",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37240206473725, 8.538308005327279],
			"icon": purple,
			"lineColor" : lineColorPurple,
			"pointernae":"Guildsman3",
			"popup":"1test",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.373861505925724, 8.538275443692807],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman31",
			"popup":"1test",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.374498887967306, 8.538794171917969],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman4",
			"popup":"1test",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37437897775108, 8.543032084097394],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman5",
			"popup":"1test",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37303201963922, 8.542779302987338],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman51",
			"popup":"1test",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37062125108082, 8.543047237954768],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman52",
			"popup":"1test",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.36910482485842, 8.543443471303119],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman6",
			"popup":"1test",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.366899129232046, 8.5458334642332],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman7",
			"popup":"1test",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.365546545956015, 8.546411220199175],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Guildsman8",
			"popup":"1test",
			"is_waypoints":"1",
			"is_all_waypoints":"0"
			
		},
		{

			"coordiates":[47.37062744456446, 8.543341570236883],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Zimmerleuten",
			"popup":"<h1>Zunfthaus zur Zimmerleuten (Builders', Carpenters', Coopers' Guild)</h1><h2>Limmatquai 40, 8001</h2><img  src='images/cha/guildsman/Zimmerleuten-L.jpg'/><p><b>Fun Tips:</b> Wrap yourself in rugs and eat an Open Air Fondue in all weathers</p><p><a href='uniwebview://action?key=https://zimmerleuten.ch/'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37103944706058, 8.543075948405097],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Rüden",
			"popup":"<h1>Haus zum Rüden (Constaffel Society)</h1><h2>Limmatquai 42, 8001</h2><img  src='images/cha/guildsman/Zum-Ruden-L.jpg'/><p><b>Fun Tips:</b> Try a Turicum gin, on tap, in the bar. See the wooden barrel-vaulted ceiling in the Gotischer Saal with a fine view over the Limmat. A male dog ('Rüde' as in 'Haus zum Rüden') with a red collar was the emblem of the Constaffel (noblemen, knights and all those Zurich citizens not members of a craft related guild). Enjoy the powerful images of the red dog in and outside the house.</p><p><a href='uniwebview://action?key=https://www.haus-zum-rueden.ch/'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37143976091109, 8.543044871033775],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Haue",
			"popup":"<h1>Zunfthaus zur Haue (Greengrocers Guild)</h1><h2>Limmatquai 52, 8001</h2><img  src='images/cha/guildsman/Zur-Haue-L.jpg'/><p><b>Fun Tips:</b> Savour the views over the Limmat river</p><p><a href='uniwebview://action?key=https://kaembel.ch/'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37155181644282, 8.543201447222962],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Saffran",
			"popup":"<h1>Zunfthaus zur Saffran (Grocers Guild)</h1><h2>Limmatquai 54, 8001</h2><img  src='images/cha/guildsman/saffran-L.jpg'/><p><b>Fun Tips:</b> Savour the views over the Limmat river</p><p><a href='uniwebview://action?key=https://saffran.ch/'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37228812507943, 8.543913345013971],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Schmiden",
			"popup":"<h1>Zunfthaus zur Schmiden (Smiths' Guild)</h1><h2>Marktgasse 20,	8001</h2><img  src='images/cha/guildsman/Schmiden-L.jpg'/><p><b>Fun Tips:</b> See the Zunftsaal</p><p><a href='uniwebview://action?key=https://www.zunfthausschmiden.ch/en/'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37021863264363, 8.54032673817528],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Waag",
			"popup":"<h1>Zunfthaus zur Waag (Weavers' Guild)</h1><h2>Münsterhof 8,	8001</h2><img  src='images/cha/guildsman/Zur-Waag-L.jpg'/><p><b>Fun Tips:</b> See the Grosse Zunftsaal, the Zunftstube and the Waagstübli </p><p><a href='uniwebview://action?key=https://www.waag.ch/'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.37003119031274, 8.541728510311152],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Meisen",
			"popup":"<h1>Zunfthaus zur Meisen (Innkeepers' Guild)</h1><h2>Münsterhof 20, 8001</h2><img  src='images/cha/guildsman/Meisen-L.jpg'/><p><b>Fun Tips:</b> Enjoy the noble architecture of the outside of the building and, while you walk around, a Vicafe espresso from the bar next door</p><p><a href='uniwebview://action?key=https://www.zunftzurmeisen.org/'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		},
		{

			"coordiates":[47.36900748475501, 8.545600493519855],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Weisser Wind",
			"popup":"<h1>Weisser Wind (Bakers' Guild)</h1><h2>Oberdorfstr. 20,	8001</h2><img  src='images/cha/guildsman/Weisser-Wind.jpg'/><p><b>Fun Tips:</b> See the modern artworks. As in the Haus zum Rüden, they feature a hunting dog, a partner for the hightime Dog perhaps.</p><p><a href='uniwebview://action?key=http://www.weggenzunft.ch/'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		,
		{

			"coordiates":[47.36978949569666, 8.539642150257329],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Savoy",
			"popup":"<h1>Hotel Savoy (Tanners' and Shoemakers' Guild)</h1><h2>Poststr. 12, 8001</h2><img  src='images/cha/guildsman/SAVOY-P.jpg'/><p><b>Fun Tips:</b> Buy a diamond or two in Harry Winston, facing the Paradeplatz on the ground floor</p><p><a href='uniwebview://action?key=http://www.gerwe-schuhmacher.ch/'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		,
		{

			"coordiates":[47.37248107551661, 8.53985460077813],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Widder",
			"popup":"<h1>Hotel Widder (Livestock Traders and Butchers' Guild)</h1><h2>Rennweg 7, 8001</h2><img  src='images/cha/guildsman/Widder-L.jpg'/><p><b>Fun Tips:</b> See the wooden Widder (ram) carvings, sit at the Social Central Table to eat in the restaurant, and enjoy the jazz</p><p><a href='uniwebview://action?key=https://www.widderhotel.com/en'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		,
		{

			"coordiates":[47.37273131037968, 8.543250647803708],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Königsstuhl ",
			"popup":"<h1>Königsstuhl (Tailors' Guild)</h1><h2>Stüssihofstatt 3, 8001</h2><img  src='images/cha/guildsman/Konigsstuhl-L.jpg'/><p><b>Fun Tips:</b> It is Thai food here in the Blue Monkey. The hightime Dog might get hot under the collar and bark at him.</p><p><a href='uniwebview://action?key=https://www.schneidern.ch/'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		,
		{

			"coordiates":[47.37131410996274, 8.541722605971348],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Storchen",
			"popup":"<h1>Hotel Storchen (Fishermens' and Boatmens' Guild)</h1><h2>Weinplatz 2, 8001</h2><img  src='images/cha/guildsman/Storchen-P.jpg'/><p><b>Fun Tips:</b> Try the Nest roof terrace or the tables outdoors in the Weinplatz</p><p><a href='uniwebview://action?key=https://www.schiffleuten.ch/'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		,
		{

			"coordiates":[47.365997798899, 8.546037303866067],
			"icon": yellow,
			"lineColor" : lineColorYellow,
			"pointernae":"Sechseläutenplatz",
			"popup":"<h1>Sechseläutenplatz (The 'Böög' is burnt here)</h1><h2>Sechseläutenplatz, 8001</h2><img  src='images/cha/guildsman/Sechselautenplatz-L.jpg'/><p><b>Fun Tips:</b> Have a drink at the Goethe Bar or the Collana, or just grab one of the metal chairs on the Sechseläutenplatz and watch the streetlife.</p><p><a href='uniwebview://action?key=https://en.wikipedia.org/wiki/Sechsel%C3%A4utenplatz,_Z%C3%Bcrich'>For more information</a></p>",
			"is_waypoints":"0",
			"is_all_waypoints":"0"
		}
		],
		"Rower":[
			{
		
					"coordiates":[47.37151131213748, 8.532697150081404],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach1",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.3708291009802, 8.532151751242234],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach2",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.37014788904486, 8.534149995334063],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach3",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.369661057184956, 8.534718621171955],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach4",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.368938063076364, 8.535201421597668],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach5",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36842667701106, 8.537419954221503],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach6",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.365850829511594, 8.539777914008242],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach61",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.366126967070684, 8.540308991384661],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach7",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36595983135235, 8.540818611089305],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach8",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.35777188953888, 8.545721573495454],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach9",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.352131690124345, 8.548253578764847],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach10",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				 {
		
					"coordiates":[47.352756349500154, 8.553149317199228],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach11",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34951489774295, 8.556879562979487],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach12",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.349867276907354, 8.560588890465732],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach13",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34514957856003, 8.559059078220912],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach14",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.33818955955939, 8.568512740832203],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach15",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34581693603414, 8.536687650259479],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach16",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36595983135235, 8.540818611089305],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach161",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36618146773389, 8.540909806194348],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach17",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.366823665854376, 8.541794506890595],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach18",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.368682585605214, 8.54199545081705],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach19",
					"popup":"1test",
					"is_waypoints":"1",
		"is_all_waypoints":"0"
				},
				{
	
				"coordiates":[47.3715325105001, 8.532413863400956],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Schanzengraben (Swim M)",
				"popup":"<h1>Schanzengraben (Swimming)</h1><h2>Badweg 10, 8001</h2><img  src='images/cha/rower/Schanzengraben-L.jpg'/><p><b>Fun Tips:</b> Try the after party in the Rimini bar in this Badi</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			  {
	
				"coordiates":[47.368682585605214, 8.54199545081705],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Stadthausquai (Swim F)",
				"popup":"<h1>Stadthausquai (Swimming)</h1><h2>Stadthausquai 12, 8001</h2><img   src='images/cha/rower/Stadthausquai-Women-L.jpg'/><p><b>Fun Tips:</b> Try the after party in the Barfuss bar in this Badi</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
		{
	
				"coordiates":[47.3618190419381, 8.536610633106191],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Enge (Swim)",
				"popup":"<h1>Enge (Swimming)</h1><h2>Mythenquai 9, 8002</h2><img  src='images/cha/rower/Enge-L.jpg'/><p><b>Fun Tips:</b> Go for the view of the alps</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.36198317757748, 8.547126150210916],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Utoquai (Swim)",
				"popup":"<h1>Utoquai (Swimming)</h1><h2>Utoquai 50, 8008</h2><img  src='images/cha/rower/Utoquai-L.jpg'/><p><b>Fun Tips:</b> Chill at the bar and tapas restaurant </p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			 {
	
				"coordiates":[47.353429934706746, 8.53450210680818],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Mythenquai (Swim)",
				"popup":"<h1>Mythenquai (Swimming)</h1><h2>Mythenquai 95, 8002</h2><img  src='images/cha/rower/Mythenquai-L.jpg'/><p><b>Fun Tips:</b> Go for the Züriseeüberquerung on July 7 in 2021 (Swimming 1.5 km across the lake from Badi Mythenquai to Badi Tiefenbrunnen)</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.35205114450637, 8.558120939558467],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Tiefenbrunnen (Swim)",
				"popup":"<h1>Tiefenbrunnen (Swimming)</h1><h2>Bellerivestr 200, 8002</h2><img  src='images/cha/rower/Tiefenbrunen-L.jpg'/><p><b>Fun Tips:</b> Try the expansive sunbathing areas on the grass and the added table tennis, water slide, and diving board</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.356610519254204, 8.536209450390956],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Boathouses",
				"popup":"<h1>Boathouses</h1><h2>Mythenquai 81, 8002</h2><img  src='images/cha/rower/Boathouses-L.jpg'/><p><b>Fun Tips:</b> See the unique city 'succulent' collection on the street side of the boathouses (4400 different varieties of succulents: plants from dry areas that store water to stay alive)</p><p><a href='uniwebview://action?key=https://openhouse-zuerich.org/orte/clubhaus-polytechniker-ruderclub/'>For more information</a></p>",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.33818955955939, 8.568512740832203],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Zollikon",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
	// 		{
	
	// 			"coordiates":[47.34117087355733, 8.537431706807455],
	// 			"icon":darkblue,
	// 			"lineColor" : lineColorDBlue,
	// 			"pointernae":"Wollishofen",
	// 			"popup":"0",
	// 			"is_waypoints":"0",
	// "is_all_waypoints":"0"
	// 		},
			{
	
				"coordiates":[47.366208894232535, 8.540591506809635],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Bürkliplatz",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.35277311798006, 8.553144533163572],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Zürichhorn",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.349867276907354, 8.560588890465732],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Tiefenbrunnen",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.34581693603414, 8.536687650259479],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Wollishofen",
				"popup":"0",
				"is_waypoints":"0",
	"is_all_waypoints":"0"
			}
		  ],
		

		"Beachgirl":[
			{
		
					"coordiates":[47.37151131213748, 8.532697150081404],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach1",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.3708291009802, 8.532151751242234],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach2",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.37014788904486, 8.534149995334063],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach3",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.369661057184956, 8.534718621171955],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach4",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.368938063076364, 8.535201421597668],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach5",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36842667701106, 8.537419954221503],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach6",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.365850829511594, 8.539777914008242],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach61",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.366126967070684, 8.540308991384661],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach7",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36595983135235, 8.540818611089305],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach8",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.35777188953888, 8.545721573495454],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach9",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.352131690124345, 8.548253578764847],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach10",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				 {
		
					"coordiates":[47.352756349500154, 8.553149317199228],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach11",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34951489774295, 8.556879562979487],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach12",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.349867276907354, 8.560588890465732],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach13",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34514957856003, 8.559059078220912],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach14",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.33818955955939, 8.568512740832203],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach15",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.34581693603414, 8.536687650259479],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach16",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36595983135235, 8.540818611089305],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach161",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.36618146773389, 8.540909806194348],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach17",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.366823665854376, 8.541794506890595],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach18",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
		
					"coordiates":[47.368682585605214, 8.54199545081705],
					"icon":darkblue,
					"lineColor" : lineColorDBlue,
					"pointernae":"Beach19",
					"popup":"1test",
					"is_waypoints":"1",
					"is_all_waypoints":"0"
				},
				{
	
				"coordiates":[47.3715325105001, 8.532413863400956],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Schanzengraben (Swim M)",
				"popup":"<h1>Schanzengraben (Swimming, Men)</h1><h2>Badweg 10, 8001</h2><img  src='images/cha/rower/Schanzengraben-L.jpg'/><p><b>Fun Tips:</b> Try the after party in the Rimini bar in this Badi</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			},
			  {
	
				"coordiates":[47.368682585605214, 8.54199545081705],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Stadthausquai (Swim F)",
				"popup":"<h1>Stadthausquai (Swimming, Women)</h1><h2>Stadthausquai 12, 8001</h2><img   src='images/cha/rower/Stadthausquai-Women-L.jpg'/><p><b>Fun Tips:</b> Try the after party in the Barfuss bar in this Badi</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			},
		{
	
				"coordiates":[47.3618190419381, 8.536610633106191],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Enge (Swim)",
				"popup":"<h1>Enge (Swimming)</h1><h2>Mythenquai 9, 8002</h2><img  src='images/cha/rower/Enge-L.jpg'/><p><b>Fun Tips:</b> Go for the view of the alps</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.36198317757748, 8.547126150210916],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Utoquai (Swim)",
				"popup":"<h1>Utoquai (Swimming)</h1><h2>Utoquai 50, 8008</h2><img  src='images/cha/rower/Utoquai-L.jpg'/><p><b>Fun Tips:</b> Chill at the bar and tapas restaurant </p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			},
			 {
	
				"coordiates":[47.353429934706746, 8.53450210680818],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Mythenquai (Swim)",
				"popup":"<h1>Mythenquai (Swimming)</h1><h2>Mythenquai 95, 8002</h2><img  src='images/cha/rower/Mythenquai-L.jpg'/><p><b>Fun Tips:</b> Go for the Züriseeüberquerung on July 7 in 2021 (Swimming 1.5 km across the lake from Badi Mythenquai to Badi Tiefenbrunnen)</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.35205114450637, 8.558120939558467],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Tiefenbrunnen (Swim)",
				"popup":"<h1>Tiefenbrunnen (Swimming)</h1><h2>Bellerivestr 200, 8002</h2><img  src='images/cha/rower/Tiefenbrunen-L.jpg'/><p><b>Fun Tips:</b> Try the expansive sunbathing areas on the grass and the added table tennis, water slide, and diving board</p><p><a href='uniwebview://action?key=https://www.zuerich.com/en/visit/the-seasons-in-zurich/summer/outdoor-pools-in-zurich'>For more information</a></p>",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.356610519254204, 8.536209450390956],
				"icon":darkblue,
				"lineColor" : lineColorDBlue,
				"pointernae":"Boathouses",
				"popup":"<h1>Boathouses</h1><h2>Mythenquai 81, 8002</h2><img  src='images/cha/rower/Boathouses-L.jpg'/><p><b>Fun Tips:</b> See the unique city 'succulent' collection on the street side of the boathouses (4400 different varieties of succulents: plants from dry areas that store water to stay alive)</p><p><a href='uniwebview://action?key=https://openhouse-zuerich.org/orte/clubhaus-polytechniker-ruderclub/'>For more information</a></p>",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.33818955955939, 8.568512740832203],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Zollikon",
				"popup":"0",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			},
	// 		{
	
	// 			"coordiates":[47.34117087355733, 8.537431706807455],
	// 			"icon":darkblue,
	// 			"lineColor" : lineColorDBlue,
	// 			"pointernae":"Wollishofen",
	// 			"popup":"0",
	// 			"is_waypoints":"0",
	// "is_all_waypoints":"0"
	// 		},
			{
	
				"coordiates":[47.366208894232535, 8.540591506809635],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Bürkliplatz",
				"popup":"0",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.35277311798006, 8.553144533163572],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Zürichhorn",
				"popup":"0",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.349867276907354, 8.560588890465732],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Tiefenbrunnen",
				"popup":"0",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			},
			{
	
				"coordiates":[47.34581693603414, 8.536687650259479],
				"icon":ferry,
				"lineColor" : lineColorDBlue,
				"pointernae":"Wollishofen",
				"popup":"0",
				"is_waypoints":"0",
				"is_all_waypoints":"0"
			}
		  ],
		  "Roadworker":[
			{
				"coordiates":[47.368470, 8.543485],
				"icon":darkblue,
				"lineColor" : lineColorcook,
				"pointernae":"Roadworker1",
				"popup":"1test",
				"is_waypoints":"1",
				"is_all_waypoints":"0"
			},
			{
				"coordiates":[47.377691, 8.535870],
				"icon":darkblue,
				"lineColor" : lineColorcook,
				"pointernae":"Roadworker2",
				"popup":"1test",
				"is_waypoints":"1",
				"is_all_waypoints":"0"
			},
			{
				"coordiates":[47.376152, 8.534052],
				"icon":darkblue,
				"lineColor" : lineColorcook,
				"pointernae":"Roadworker3",
				"popup":"1test",
				"is_waypoints":"1",
				"is_all_waypoints":"0"
			},
			// {
			// 	"coordiates":[47.369810, 8.536781],
			// 	"icon":darkblue,
			// 	"lineColor" : lineColorcook,
			// 	"pointernae":"Roadworker4",
			// 	"popup":"1test",
			// 	"is_waypoints":"1",
			// 	"is_all_waypoints":"0"
			// },
		  ]
	  
	}

var greenIcon = new LeafIcon({iconUrl: 'https://animateyourcity.com/green.png'});	
		
var url_string = window.location.href; //window.location.href
var url = new URL(url_string);
var character = url.searchParams.get("character");


const cat = character;
const waypointarray = [];
const waypointarray1 = [];
for (x in categorys) {
	

	if(x == cat || cat == 'all'){ 
		
		var lineColor = cat =="all" ? "black" : categorys[x][0].lineColor;
		categorys[x].forEach(function(value,key){

			if(value.is_waypoints == "1" && cat != 'all' ){ // While Run category when not come all

			waypointarray.push(L.latLng(value.coordiates[0],value.coordiates[1]));
			}

			if(value.is_all_waypoints != "0" && cat == 'all' ){ // WHile run category is All

			waypointarray.push(L.latLng(value.coordiates[0],value.coordiates[1]));
			}

			if(value.is_all_waypoints == "2" && cat == 'all' ){ // WHile run category is All

			waypointarray1.push(L.latLng(value.coordiates[0],value.coordiates[1]));
			}
	
		})
	}
	
}

for (x in categorys) {
	console.log(cat);

	if(x == cat || cat == 'all'){ 
		
		categorys[x].forEach(function(value,key){

			if(value.is_waypoints == "0"){

				//console.log(value.coordiates);
					L.marker(value.coordiates, {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="'+value.icon+'"/>'+
              '<div class="my-div-span">'+value.pointernae+'</div>'
    })}).bindPopup(value.popup).addTo(map);

	}
		if(value.popup == "0") {
			console.log(value.coordiates);
		}
	})
	}
  }

  var control = L.Routing.control({
	/* Pass in the 1st and last coords in the selected list as waypoints.
	   This Routing sets the initial zoom level of the map.
	   DO not delete this unless you have an alternative */
	waypoints:[waypointarray[0],waypointarray[waypointarray.length-1]], 
	geocoder: L.Control.Geocoder.nominatim(),
	waypointMode: "snap",
	//fitSelectedRoutes: "smart",
	lineOptions: {
      styles: [{color: lineColor, opacity: 0, weight: 0}]
   },
	createMarker: function() { return null; }

}).addTo(map);


  var firstpolyline = new L.Polyline(waypointarray, {
    color: lineColor,
    weight: 3,
    opacity: 1,
    smoothFactor: 1
});
firstpolyline.addTo(map);

if(cat == 'Roadworker'){
	var latlngs = [
		[47.368470, 8.543485],
		[47.368493, 8.543570],
		[47.367579, 8.544027],
		[47.367566, 8.543932]
	];

	var latlngs1 = [
		[47.377691, 8.535870],
		[47.378258, 8.536337],
		[47.377842, 8.537844],
		[47.377811, 8.537828],
		[47.377726, 8.538155],
		[47.377701, 8.538134],
		[47.377785, 8.537811],
		[47.377788, 8.537755],
		[47.377732, 8.537577],
		[47.377679, 8.537532],
		[47.377703, 8.537485],
		[47.377738, 8.537493],
		[47.377966, 8.536683],
		[47.377952, 8.536573],
		[47.377529, 8.536200]
	];

	var latlngs2 = [
		[47.371872, 8.534170],
		[47.371706, 8.534219],
		[47.371217, 8.534687],
		[47.371204, 8.534648],
		[47.371003, 8.534832],
		[47.370985, 8.534794],
		[47.370776, 8.535058],
		[47.370710, 8.535099],
		[47.370651, 8.535203],
		[47.370650, 8.535264],
		[47.370545, 8.535442],
		[47.370626, 8.535567],
		[47.370726, 8.535402],
		[47.370777, 8.535384],
		[47.370841, 8.535278],
		[47.370836, 8.535214],
		[47.371091, 8.534860],
		[47.371748, 8.534262],
		[47.371868, 8.534231]		
	];

	var latlngs3 = [
		[47.369810, 8.536781],
		[47.369642, 8.537002],
		[47.369660, 8.537094],
		[47.369789, 8.536942],
		[47.369837, 8.536821]
	];

	var latlngs4 = [
		[47.369226, 8.537599],
		[47.369204, 8.537637],
		[47.369211, 8.537659],
		[47.369086, 8.537869],
		[47.369094, 8.537885],
		[47.369221, 8.537678],
		[47.369233, 8.537692],
		[47.369260, 8.537645]		
	];

	var latlngs5 = [
		[47.368081, 8.538791],		
		[47.368022, 8.539021],		
		[47.367720, 8.539260],		
		[47.367672, 8.539031],		
		[47.367747, 8.538966],		
		[47.367774, 8.539061],		
		[47.368081, 8.538784]			
	];

	var latlngs6 = [
		[47.366749, 8.540007],		
		[47.366770, 8.540067],		
		[47.366433, 8.540452],		
		[47.366386, 8.540336]			
	];
	
	var polygon = L.polygon(latlngs, {color: 'brown'});
	polygon.setStyle({fillColor: 'brown',opacity:1});
	polygon.addTo(map);
	
	var polygon1 = L.polygon(latlngs1, {color: 'brown'});
	polygon1.setStyle({fillColor: 'brown',opacity:1});
	polygon1.addTo(map);

	var polygon1 = L.polygon(latlngs2, {color: 'brown'});
	polygon1.setStyle({fillColor: 'brown',opacity:1});
	polygon1.addTo(map);

	var polygon1 = L.polygon(latlngs3, {color: 'brown'});
	polygon1.setStyle({fillColor: 'brown',opacity:1});
	polygon1.addTo(map);

	var polygon1 = L.polygon(latlngs4, {color: 'brown'});
	polygon1.setStyle({fillColor: 'brown',opacity:1});
	polygon1.addTo(map);

	var polygon1 = L.polygon(latlngs5, {color: 'brown'});
	polygon1.setStyle({fillColor: 'brown',opacity:1});
	polygon1.addTo(map);

	var polygon1 = L.polygon(latlngs6, {color: 'brown'});
	polygon1.setStyle({fillColor: 'brown',opacity:1});
	polygon1.addTo(map);
	
	}

/*
console.log('waypointarray2');
console.log(waypointarray1);
console.log('waypointarray1');

  var firstpolyline1 = new L.Polyline(waypointarray1, {
    color: lineColor,
    weight: 4,
    opacity: 1,
    smoothFactor: 1
});
firstpolyline1.addTo(map);*/


/*	L.marker([47.3766937, 8.5403454], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="https://animateyourcity.com/green.png"/>'+
              '<div class="my-div-span">Pointer</div>'
    })}).bindPopup("I am a green leaf.").addTo(map);



	L.marker([47.3704873, 8.5354656], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="'+green+'"/>'+
              '<div class="my-div-span">'+green+'</div>'
    })}).bindPopup(green).addTo(map);
	/*L.marker([47.3742229, 8.5335609], {icon: orangeIcon}).bindPopup("I am an orange leaf1.").addTo(map);*/
	/*L.marker([47.3753395, 8.5363169], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="https://animateyourcity.com/green.png"/>'+
              '<div class="my-div-span">Pointer2</div>'
    })}).bindPopup("I am an orange leaf2.").addTo(map);
	L.marker([47.377052, 8.5393448], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="https://animateyourcity.com/green.png"/>'+
              '<div class="my-div-span">Pointer3</div>'
    })}).bindPopup("I am an orange leaf3.").addTo(map);
    	L.marker([47.3686341, 8.5367769], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="https://animateyourcity.com/green.png"/>'+
              '<div class="my-div-span">Pointer4</div>'
    })}).bindPopup("<h1>I am an orange leaf3.</h1>").addTo(map);
        	L.marker([47.3721811, 8.5413182], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="https://animateyourcity.com/green.png"/>'+
              '<div class="my-div-span">Pointer5</div>'
    })}).bindPopup("I am an orange leaf3.").addTo(map);
        	L.marker([47.3637311, 8.5315812], {icon: new L.DivIcon({
        className: 'my-div-icon',
        html: '<img class="my-div-image" src="https://animateyourcity.com/green.png"/>'+
              '<div class="my-div-span">FIFA World Football Museum</div>'
    })}).bindPopup("<h1>FIFA World Football Museum</h1><br><img src='images/cha/danndy/GLOBUS L.jpg'/>").addTo(map);
L.Routing.errorControl(control).addTo(map);
*/


